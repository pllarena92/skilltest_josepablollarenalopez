<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movie';

    public function funciones(){
        return $this->hasMany('App\Funcion');
    }
}
