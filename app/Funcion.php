<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcion extends Model
{
    protected $table = 'funciones';

    protected $fillable = [
        'movieId', 'time'
    ];

    public function movie(){
        return $this->belongsTo('App\Movie','movieId');
    }
}
