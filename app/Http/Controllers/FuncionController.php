<?php

namespace App\Http\Controllers;

use App\Funcion;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FuncionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $funciones = Funcion::all();
        $movies = Movie::all();

        return view('funciones.index', array(
            "funciones"=>$funciones,
            "movies"=>$movies
        ));
    }

    public function register(Request $request){
        $validate = $this->validate($request, [
            'movieId' => ['required'],
            'time' => ['required']
        ]);

        if($request->ajax()){
            DB::table('funciones')->insert([
               'movieId' => $request->input('movieId'),
               'time'=>$request->input('time')
            ]);
            return response()->json(['message' => 'Insertado correctamente']);
        }

    }

    public function seleccionaFuncion()
    {
        $user = \Auth::user();

        $funcionesSel = DB::table('usuariofunciones')->where('userId', '=', $user->id);

        $funciones = DB::table('funciones')->get()->all();
        $asientos = DB::table('seat')->get()->all();

        return view('funciones.selecciona', array(
            "selecciones"=>$funcionesSel,
            "funciones"=>$funciones,
            "asientos"=>$asientos
        ));
    }
}
