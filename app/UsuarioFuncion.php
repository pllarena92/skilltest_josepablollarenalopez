<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioFuncion extends Model
{
    protected $table =  'usuariofunciones';

    public function movies(){
        return $this->hasMany('App\Movie');
    }

    public function funciones(){
        return $this->hasMany('App\Funcion');
    }

    public function user(){
        return $this->belongsTo('App\User', 'userId');
    }
}
