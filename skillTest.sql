# Volcado de tabla movie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie`;

CREATE TABLE `movie` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `img` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;

INSERT INTO `movie` (`id`, `name`, `img`)
VALUES
	(1,'Good Will Hunting','1.jpg'),
	(2,'The Dark Knight Rises','2.jpg'),
	(3,'The Hobbit: The Desolation of Smaug','3.jpg');

/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla seat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `seat`;

CREATE TABLE `seat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seat` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `seat` WRITE;
/*!40000 ALTER TABLE `seat` DISABLE KEYS */;

INSERT INTO `seat` (`id`, `seat`)
VALUES
	(1,'A1'),
	(2,'A2'),
	(3,'A3'),
	(4,'A4'),
	(5,'A5'),
	(6,'A6'),
	(7,'A7'),
	(8,'A8'),
	(9,'A9'),
	(10,'A10'),
	(11,'B1'),
	(12,'B2'),
	(13,'B3'),
	(14,'B4'),
	(15,'B5'),
	(16,'B6'),
	(17,'B7'),
	(18,'B8'),
	(19,'B9'),
	(20,'B10');

/*!40000 ALTER TABLE `seat` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(60) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `role`)
VALUES
	(1,'Administrator','Lastname','admin@email.com','$2y$10$xpWyfo5x8DjfeRU2NI5.UOWAMmhHPoNHEur7hHgI6/uHbT9zuFkXi',1),
	(2,'User1','Lastname1','test1@email.com','$2y$10$xpWyfo5x8DjfeRU2NI5.UOWAMmhHPoNHEur7hHgI6/uHbT9zuFkXi',2),
	(3,'User2','Lastname2','test2@email.com','$2y$10$xpWyfo5x8DjfeRU2NI5.UOWAMmhHPoNHEur7hHgI6/uHbT9zuFkXi',2);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


//Cambios agregados a la base
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `examen_cine`.`movie` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `examen_cine`.`seat` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `examen_cine`.`users` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

CREATE TABLE IF NOT EXISTS `examen_cine`.`funciones` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `movieId` INT(11) NULL DEFAULT NULL,
  `time` TIME NULL DEFAULT NULL,
  `location` TEXT NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `movie_funciones_fk_idx` (`movieId` ASC) ,
  CONSTRAINT `movie_funciones_fk`
    FOREIGN KEY (`movieId`)
    REFERENCES `examen_cine`.`movie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `examen_cine`.`usuarioFunciones` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userId` INT(11) NULL DEFAULT NULL,
  `movieId` INT(11) NULL DEFAULT NULL,
  `seatId` INT(11) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_usuariofunciones_fk_idx` (`userId` ASC) ,
  INDEX `movie_usuariofunciones_fk_idx` (`movieId` ASC) ,
  INDEX `seat_usuariofunciones_fk_idx` (`seatId` ASC) ,
  CONSTRAINT `user_usuariofunciones_fk`
    FOREIGN KEY (`userId`)
    REFERENCES `examen_cine`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `movie_usuariofunciones_fk`
    FOREIGN KEY (`movieId`)
    REFERENCES `examen_cine`.`movie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `seat_usuariofunciones_fk`
    FOREIGN KEY (`seatId`)
    REFERENCES `examen_cine`.`seat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


ALTER TABLE `examen_cine`.`users` 
ADD COLUMN `remember_token` TEXT NULL AFTER `role`,
ADD COLUMN `created_at` TIMESTAMP NULL AFTER `remenber_token`,
ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `created_at`;

