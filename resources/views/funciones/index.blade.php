@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3>Listado de funciones</h3>
                    </div>
                    <div class="card-body">
                        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#create" style="padding-bottom: 5px; margin-bottom: 20px;">Crear nueva función</button>

                        <table class="table table-bordered">
                            <thead>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Hora</th>
                                <th>Ubicación</th>
                            </thead>
                            <tbody>
                                @foreach($funciones as $funcion)
                                    <tr>
                                        <td>{{$funcion->id}}</td>
                                        <td>{{$funcion->movie->name}}</td>
                                        <td>{{$funcion->time}}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="modal fade" id="create">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3>Crear nueva función</h3>
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span>×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('funcion.add') }}" method="post" id="frmFuncion">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="movies" class="col-md-4 col-form-label txt-md-right">Películas</label>
                                                <div class="col-md-6">
                                                    <select class="form-control{{$errors->has('movieId') ? 'is-invalid' : '' }}"  name="movieId" id="movieId">
                                                       <option value="">Seleccione una película</option>
                                                        @foreach($movies as $movie)
                                                            <option value="{{$movie->id}}">{{$movie->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="time" class="col-md-4 col-form-label txt-md-right">Horario</label>
                                                <div class="col-md-6">
                                                    <input type="time" class="form-control{{$errors->has('time') ? 'is-invalid' : ''}}" name="time" id="time">
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <div style="width: 600px; height: 400px;" id="map_canvas"></div>

                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button class="btn btn-primary" id="btn-frm" onclick="guardar()">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function guardar() {
            $("#btn-frm").click(function () {

                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                var form = $("#frmFuncion");
                var url = form.attr('action');
                var method = form.attr('method');

                $.ajax({
                    type: method,
                    url: url,
                    data: form.serialize(),
                    success: function () {
                        alert('Función agregada correctamente');
                        location.reload();
                    },
                    error: function () {
                        alert('Error al intentar agregar la función');
                    }
                });

            })
        }
    </script>

@endsection
