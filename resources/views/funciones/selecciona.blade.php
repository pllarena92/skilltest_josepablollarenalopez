@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3>Funciones seleccionadas</h3>
                    </div>
                    <div class="card-body">
                        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#create" style="padding-bottom: 5px; margin-bottom: 20px;">Seleccionar función</button>

                        <table class="table table-bordered">
                            <thead>
                            <th>ID</th>
                            <th>Función</th>
                            <th>Asiento</th>
                            <th>Hora</th>
                            <th>Ubicación</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
