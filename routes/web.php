<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home')

Route::get('/funciones', 'FuncionController@index')->name('funcion.index');

Route::post('funciones/register', 'FuncionController@register' )->name('funcion.add');

Route::get('/funciones/selecciona', 'FuncionController@seleccionaFuncion')->name('funcion.selecciona');
